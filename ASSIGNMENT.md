**Informace k úkolu**

* Existující rozhraní ani třídy neměňte!
* Metody VÝJIMEČNĚ NEMUSÍTE dokumentovat pomocí JavaDoc komentářů.
* Dbejte na pěkný kód:
  * Používejte zásadně privátní atributy.
  * Vyhněte se zbytečnému opakování kódu.
  * Vhodně a konzistentně kód "odskakujte".
  * Kód vizuálně oddělujte, například prázdným řádkem mezi metodami.
* **Pokud kód nejde v okamžiku odevzdání přeložit, tak je zkouška automaticky za 0 bodů!**


* K dispozici máte rozhraní `Chargeable` a třídu `Demo`.
* Rozhraní `Chargeable` popisuje bateriová zařízení, čili zařízení schopná se nabíjet a používat (vybíjet).
  A protože jsou to zařízení na baterie, umožňují také hlásit aktuální stav nabití (kapacitu) své baterie.
* Třída `Demo` ukazuje, jak by se měl váš kód používat. Umožňuje Vám vizuální kontrolu funkčnosti Vašeho kódu.
  Dále Vás přesně navádí jaké metody budou Vaše třídy potřebovat, typy parametrů, atd.


**Zadání:**

* Vytvořte třídy `MobilePhone` (telefon) a `Camera` (fotoaparát).
  * Telefon i fotoaparát je možné používat a nabíjet, jsou to tzv. *chargeable* přístroje. Telefon ale navíc umí **ještě posílat zprávy**, viz níže.
  * Telefon se běžně dodává (*vytváří*) s baterií umožňující dobu výdrže (kapacitu) 500 minut, fotoaparát zase 1200 minut.
  * Definujte tyto hodnoty jako veřejné statické konstanty ve svých třídách.
  * Výrobci obou přístrojů ale nabízejí k přikoupení další baterie o různých kapacitách, je tak možné koupit (*vytvořit*)
    tyto přístroje s vlastní velikostí (kapacitou) baterie. Záporná velikost baterie je pochopitelně špatně.
  * Metoda `toString()` bude u telefonu vracet "Mobile Phone has currently \<CAPACITY\> minutes of electric power.", kde \<CAPACITY\> je současný stav přístroje.
  * Metoda `toString()` bude u fotoaparátu vracet "Camera has currently \<CAPACITY\> minutes of electric power.", kde \<CAPACITY\> je současný stav přístroje.

* Vyhněte se opakování kódu tím, že pro telefon i fotoaparát vytvoříte společnou **abstraktní** nadtřídu `GenericBatteryDevice`.
  * Třída reprezentuje obecný (nekonkrétní) *chargeable* přístroj o jedné baterii.
  * Třída by měla řešit společné funkce libovolného bateriového přístroje, tedy implementovat nabíjecí a vybíjecí metody.
  * Nezapomeňte, že baterie nemůže uložit více kapacity než na kolik byla vyrobena, také nejde "pod-vybít".
  * Další vyžadované chování (metoda `toString()`) už musí mít každý přístroj naprogramované sám u sebe.

* Vytvořte rozhraní `Messaging`, které bude reprezentovat možnost odesílání SMS zpráv.
  * Rozhraní bude mít jedinou metodu: `void sendSMS(String message, int phoneNo)`
  * Přístroj, který umí dělat *messaging*, vypíše zprávu "Sending SMS to \<phoneNo\> with text: \<message\>" na std. výstup.

* Nakonec vytvořte třídu `DeviceTester` s následujícími **statickými** metodami:
  * `dischargeTest()` vezme objekt schopný nabíjení a počet minut pro používání přístroje jako svoje vstupní argumenty.
    * Tento objekt pak nechá vybít -- metoda `use()`.
    * Pokud se přístroj vybije úplně, pak vypíše na standardní výstup: "Device was fully discharged."
    * V opačném případě vypíše na standardní výstup "\<OBJ\> has currently \<CAPACITY\> minutes of electric power."
      -- což přesně dělají metody `toString()` takovýchto objektů.
  * `messageTest()` vezme objekt schopný zasílání zpráv, text zprávy a telefonní číslo (int) jako svoje vstupní argumenty.
    * Skrz tento objekt se potom pošle zadaná zpráva na zadané číslo -- metoda `sendSMS()`.
  * Nemusíte kontrolovat *nullovost* vstupních argumentů.
  * Metody mají být statické, protože nepotřebují nic ze své třídy `DeviceTester`.


**Kontrola:**

Třída `Demo` slouží pro vaši kontrolu funkčnosti kódu. Správný výstup by měl tedy vypadat následovně:

<pre>
Mobile Phone has currently 500 minutes of electric power.
Using Mobile Phone for playing Snake for 300 minutes -> Success.
Using Mobile Phone for playing Snake for another 300 minutes without charging -> Discharged.
Mobile Phone has currently 0 minutes of electric power.

Charging Mobile Phone for 1000 minutes.
Using Mobile Phone for playing Snake again for 10 minutes -> Success.
Mobile Phone has currently 490 minutes of electric power.
Sending SMS to 777666555 with text: Test SMS

Testing camera
Camera has currently 400 minutes of electric power.
Device was fully discharged.
</pre>
