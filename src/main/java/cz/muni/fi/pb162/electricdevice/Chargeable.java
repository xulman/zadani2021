package cz.muni.fi.pb162.electricdevice;

/**
 * Interface for battery powered devices that can be charged or used (discharged).
 *
 * @author Rabbit Duracell
 */
public interface Chargeable {

    /**
     * Returns the currently available capacity, it is expressed as minutes of operation before the device is fully discharged.
     * 
     * @return current available capacity of the device in minutes
     */
    int getAvailableCapacity();

    /**
     * Charges the device such that its operation time is prolonged by the given amount of minutes.
     * Important note: Every device has maximum capacity and cannot be charged beyond this capacity.
     * 
     * @param minutes operational time that device can gain as a result of being charged
     */
    void charge(int minutes);

    /**
     * Uses (and discharges) the device for the given amount of time (in minutes).
     * Important note: Of course, the actual usage time cannot exceed the capacity of the device's battery.
     *
     * @param minutes operational time for which an user wants to use this device
     * @return true if the device was operating for the given time,
     *         false if the device got discharged during the wanted period
     */
    boolean use(int minutes);

    @Override
    String toString();
}
