package cz.muni.fi.pb162.electricdevice;

/**
 * A class to demonstrate the intended use of this package.
 *
 * @author User Hypothetical
 */
public class Demo {

    public static void main(String[] args) {
        //phone with standard battery
        final MobilePhone nokia5110 = new MobilePhone();

        //1st usage example
        System.out.println(nokia5110);
        System.out.print("Using Mobile Phone for playing Snake for 300 minutes ->");
        System.out.println(nokia5110.use(300) ? " Success." : " Discharged.");
        System.out.print("Using Mobile Phone for playing Snake for another 300 minutes without charging ->");
        System.out.println(nokia5110.use(300) ? " Success." : " Discharged.");
        System.out.println(nokia5110);

        //2nd usage example
        System.out.println("");
        System.out.println("Charging Mobile Phone for 1000 minutes.");
        nokia5110.charge(1000);
        System.out.print("Using Mobile Phone for playing Snake again for 10 minutes ->");
        System.out.println(nokia5110.use(10) ? " Success." : " Discharged.");
        System.out.println(nokia5110);

        //3rd usage example
        DeviceTester.messageTest(nokia5110, "Test SMS", 777666555);

        //camera with own battery
        final Camera cannon = new Camera(1100);

        //1st usage example
        System.out.println("");
        System.out.println("Testing camera");
        DeviceTester.dischargeTest(cannon, 700);
        DeviceTester.dischargeTest(cannon, 700);
    }
}